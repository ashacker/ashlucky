const HTTP_URL_APIJSON = 'http://127.0.0.1:8888/';
const HTTP_URL = 'http://www.ashacker.com:8088/lucky/app/';

const HTTPS_URL = 'https://www.ashacker.com/api/app/';

const METHOD_GET = 'get/';

const METHOD_LOGIN = 'userLogin';
const METHOD_VALID_CODE = 'validCode';
const METHOD_VALID_PHONE = 'validPhone';
const METHOD_REGIST = 'userRegister';
const METHOD_GOODS = 'getGoodsListByType?';
// const 

function friendlyDate(timestamp) {
	var formats = {
		'year': '%n% 年前',
		'month': '%n% 月前',
		'day': '%n% 天前',
		'hour': '%n% 小时前',
		'minute': '%n% 分钟前',
		'second': '%n% 秒前',
	};

	var now = Date.now();
	var seconds = Math.floor((now - timestamp) / 1000);
	var minutes = Math.floor(seconds / 60);
	var hours = Math.floor(minutes / 60);
	var days = Math.floor(hours / 24);
	var months = Math.floor(days / 30);
	var years = Math.floor(months / 12);

	var diffType = '';
	var diffValue = 0;
	if (years > 0) {
		diffType = 'year';
		diffValue = years;
	} else {
		if (months > 0) {
			diffType = 'month';
			diffValue = months;
		} else {
			if (days > 0) {
				diffType = 'day';
				diffValue = days;
			} else {
				if (hours > 0) {
					diffType = 'hour';
					diffValue = hours;
				} else {
					if (minutes > 0) {
						diffType = 'minute';
						diffValue = minutes;
					} else {
						diffType = 'second';
						diffValue = seconds === 0 ? (seconds = 1) : seconds;
					}
				}
			}
		}
	}
	return formats[diffType].replace('%n%', diffValue);
}


async function doValidCode(phone) {
	var url = HTTP_URL + METHOD_VALID_CODE;
	var params = '?phone_num=' + phone;
	return doHTTPNetwork(url, params);
}

async function doValidPhone(phone, code) {
	var url = HTTP_URL + METHOD_VALID_PHONE;
	var params = '?phone_num=' + phone + '&sms_code=' + code;
	return doHTTPNetwork(url, params);
}

async function doRegist(phone, pwd,token) {
	var url = HTTP_URL + METHOD_REGIST;
	var params = '?phone_num=' + phone + '&user_pwd=' + pwd+'&user_token'+token;
	return doHTTPNetwork(url, params);
}

async function queryUser(phone, pwd,token) {
	var url = HTTP_URL + METHOD_LOGIN;
	var params = '?phone_num=' + phone + '&user_pwd=' + pwd+'&user_token'+token;
	return doHTTPNetwork(url, params);
}

async function doLogin(phone, pwd) {
	var url = HTTP_URL + METHOD_LOGIN;
	var params = '?phone_num=' + phone + '&user_pwd=' + pwd;
	return doHTTPNetwork(url, params);
}

async function queryTodayAction() {
	// var url = HTTP_URL_APIJSON+METHOD_GET;
	// var params = '{"NewsModel":{"goods_flag":3}}';
	//  return queryAction(params);
	
	var url = HTTP_URL + METHOD_GOODS;
	var params = 'page_start=' + 0 + '&page_size=' + 1 + '&goods_flag=3';
	return new Promise(function(resolve, reject) {
		doHTTPNetwork(url, params).then(function(data) {
			console.log('queryTodayAction:' + data);
			let arr = data['list'];
			let goods = arr[0];
			console.log(arr);
			const arrdata =  {
					id: goods.id,
					type: goods.id,
					datetime: goods.createTime,
					// datetime: friendlyDate(new Date(news.published_at.replace(/\-/g, '/')).getTime()),
					title: goods.goodsTitle + '~~~' + goods.goodsSummary, //'我们的小幸运，随便写点啥，就是想写点啥，不知道写点啥，写个鞋垫吧！！！',
					image_url: goods.goodsCover,
					source: 'Lucky',
					comment_count: 0,
					info:goods.goodsDesc
					// post_id: goods.id
				}; 
 
			resolve(arrdata);
		});

	});
}



async function queryTommorrowAction() {
	// var url = HTTP_URL_APIJSON + METHOD_GET;
	// var params = '{"NewsModel":{"goods_flag":5}}';
	// return queryAction(params);
	
	
	var url = HTTP_URL + METHOD_GOODS;
	var params = 'page_start=' + 0 + '&page_size=' + 1 + '&goods_flag=5';
	return new Promise(function(resolve, reject) {
		doHTTPNetwork(url, params).then(function(data) {
			console.log('queryTodayAction:' + data);
			let arr = data['list'];
			let goods = arr[0];
			console.log(arr);
			const arrdata =  {
					id: goods.id,
					type: goods.id,
					datetime: goods.createTime,
					// datetime: friendlyDate(new Date(news.published_at.replace(/\-/g, '/')).getTime()),
					title: goods.goodsTitle + '~~~' + goods.goodsSummary, //'我们的小幸运，随便写点啥，就是想写点啥，不知道写点啥，写个鞋垫吧！！！',
					image_url: goods.goodsCover,
					source: 'Lucky',
					comment_count: 0,
					info:goods.goodsDesc
					// post_id: goods.id
				}; 
	 
			resolve(arrdata);
		});
	
	});
}

async function queryPastActions() {
	// var url = HTTP_URL_APIJSON+METHOD_GET;
	// var params = '{"NewsModel[]": {"NewsModel":{"goods_flag":4}}}';
	// return queryAction(params);
	// 商品标识：1已过期 2未开始 3已开始 4已结束 5明日活动 6未上线已创建 0删除

	var url = HTTP_URL + METHOD_GOODS;
	var params = 'page_start=' + 0 + '&page_size=' + 100 + '&goods_flag=4';
	return new Promise(function(resolve, reject) {
		doHTTPNetwork(url, params).then(function(data) {
			console.log('queryPastActions:' + data);
			let arr = data['list'];

			console.log(arr);
			// activeTab.data = data;  
			// tdata = data['NewsModel'];

			// ttt = 'ssdddds';
			const arrdata = arr.map((goods) => {
				return {
					id: 1,
					newsid: goods.id,
					article_type: goods.id,
					datetime: goods.createTime,
					// datetime: friendlyDate(new Date(news.published_at.replace(/\-/g, '/')).getTime()),
					title: goods.goodsTitle + '~~~' + goods.goodsSummary, //'我们的小幸运，随便写点啥，就是想写点啥，不知道写点啥，写个鞋垫吧！！！',
					image_url: goods.goodsCover,
					source: 'Lucky',
					comment_count: 0,
					info:goods.goodsDesc,
					post_id: goods.id
				};
			});
			// resolve(arrdata);
			// return new Promise(function (resolve,reject) {
			console.log('Promise 2');
			console.log(arrdata);
			resolve(arrdata);
		});

	});
	// return doHTTPNetwork(params);
}


async function doHTTPSNetwork(url, params) {
	// var url = url+METHOD_GET;
	// var params = '{"NewsModel":{"goods_flag":5}}';
	return new Promise(function(resolve, reject) {
		uni.request({
			url: url + params,
			// data: params,
			success: (result) => {
				if (result.statusCode !== 200) {
					reject(result);
					return;
				}

				const data = result.data
				console.log(url + params);
				console.log(data);
				resolve(data);
			},
			fail: (err) => {
				reject(err);
				uni.showModal({
					content: err.errMsg,
					showCancel: false
				})
			},
			complete: (e) => {
				// activeTab.isLoading = false;
				reject('stop');
			}
		});

	});
}


async function doHTTPNetwork(url, params) {
	// var url = url+METHOD_GET;
	// var params = '{"NewsModel":{"goods_flag":5}}';
	console.log(url + params);
	return new Promise(function(resolve, reject) {
		uni.request({
			url: url + params,
			// data: params,
			success: (result) => {
				// if (result.statusCode !== 200) {
				// 	reject(result);
				// 	return;
				// }

				const data = result.data
				console.log(url + params);
				console.log(data);
				resolve(data);
			},
			fail: (err) => {
				reject(err);
				uni.showModal({
					content: err.errMsg,
					showCancel: false
				})
			},
			complete: (e) => {
				// activeTab.isLoading = false;
				reject('stop');
			}
		});

	});
}

async function doHTTPNetworkPOST(url, params) {
	// var url = url+METHOD_GET;
	// var params = '{"NewsModel":{"goods_flag":5}}';
	console.log(url + params);
	return new Promise(function(resolve, reject) {
		uni.request({
			url: url + params,
			header:{
				'content-type':'application/json' 
				// 'Access-Control-Allow-Origin':'*'
			},
			// data: params,
			success: (result) => {
				// if (result.statusCode !== 200) {
				// 	reject(result);
				// 	return;
				// }

				const data = result.data
				console.log(url + params);
				console.log(data);
				resolve(data);
			},
			fail: (err) => {
				reject(err);
				uni.showModal({
					content: err.errMsg,
					showCancel: false
				})
			},
			complete: (e) => {
				// activeTab.isLoading = false;
				reject('stop');
			}
		});

	});
}

async function queryAction(params) {
	var url = HTTP_URL_APIJSON + METHOD_GET;
	// var params = '{"NewsModel":{"goods_flag":5}}';
	return new Promise(function(resolve, reject) {
		uni.request({
			url: url + params,
			// data: params,
			success: (result) => {
				if (result.statusCode !== 200) {
					reject(result);
					return;
				}

				const data = result.data
				console.log(url + params);
				console.log(data);
				resolve(data);
			},
			fail: (err) => {
				reject(err);
				uni.showModal({
					content: err.errMsg,
					showCancel: false
				})
			},
			complete: (e) => {
				// activeTab.isLoading = false;
				reject('stop');
			}
		});

	});
}



//  async function queryTodayAction2(url,params,type){
// 	 
// 	return new Promise(function(resolve, reject){
// 	 	
// 	uni.request({
// 		url: url+params,
// 		 // data: params,
// 		success: (result) => {
// 			
// 			if (result.statusCode !== 200) {
// 				return;
// 			}
// 			// const data = result.data["NewsModel[]"];
// 			// const data = result.data["NewsModel[]"].map((news) => {
// 			// 	 
// 			// 	return {
// 			// 		id:1,
// 			// 		newsid: news.id,
// 			// 		article_type: news.goods_flag,
// 			// 		datetime:news.create_time,
// 			// 		// datetime: friendlyDate(new Date(news.published_at.replace(/\-/g, '/')).getTime()),
// 			// 		title:  news.goods_title+news.goods_desc,//'我们的小幸运，随便写点啥，就是想写点啥，不知道写点啥，写个鞋垫吧！！！',
// 			// 		image_url:"https://assets.logitech.com/assets/65776/73/mx-master-3.png",
// 			// 		source: 'Lucky' ,
// 			// 		comment_count: 0,
// 			// 		post_id: news.post_id
// 			// 	};
// 			// });
// 			 const data =  result.data
// 			console.log(url+params);
// 			console.log(data);
// 			resolve(data);
// 			
// 			// return data;
// 			// if (action === 1) {
// 			// 	activeTab.data = data;
// 			// 	this.refreshing = false;
// 			// } else {
// 			// 	data.forEach((news) => {
// 			// 		activeTab.data.push(news);
// 			// 	});
// 			// }
// 			// activeTab.requestParams.minId = data[data.length - 1].newsid;
// 		},
// 		fail: (err) => {
// 			reject(err);
// 			uni.showModal({
// 				content: err.errMsg,
// 				showCancel: false
// 			})
// 		},
// 		complete: (e) => {
// 			// activeTab.isLoading = false;
// 		}
// 	});
// 	
// 	});
// }
// async function netHelper(url,params,type){
// 	 
// 	return new Promise(function(resolve, reject){
// 	 	
// 	uni.request({
// 		// url: 'https://unidemo.dcloud.net.cn/api/news',
// 		url: url+params,//'http://127.0.0.1:8888/get/',
// 		 // data: params,
// 		success: (result) => {
// 			
// 			if (result.statusCode !== 200) {
// 				return;
// 			}
// 			// const data = result.data["NewsModel[]"];
// 			const data = result.data["NewsModel[]"].map((news) => {
// 				 
// 				return {
// 					id:1,
// 					newsid: news.id,
// 					article_type: news.goods_flag,
// 					datetime:news.create_time,
// 					// datetime: friendlyDate(new Date(news.published_at.replace(/\-/g, '/')).getTime()),
// 					title:  news.goods_title+news.goods_desc,//'我们的小幸运，随便写点啥，就是想写点啥，不知道写点啥，写个鞋垫吧！！！',
// 					image_url:"https://assets.logitech.com/assets/65776/73/mx-master-3.png",
// 					source: 'Lucky' ,
// 					comment_count: 0,
// 					post_id: news.post_id
// 				};
// 			});
// 			console.log(url+params);
// 			console.log(data);
// 			resolve(data);
// 			
// 			// return data;
// 			// if (action === 1) {
// 			// 	activeTab.data = data;
// 			// 	this.refreshing = false;
// 			// } else {
// 			// 	data.forEach((news) => {
// 			// 		activeTab.data.push(news);
// 			// 	});
// 			// }
// 			// activeTab.requestParams.minId = data[data.length - 1].newsid;
// 		},
// 		fail: (err) => {
// 			reject(err);
// 			uni.showModal({
// 				content: err.errMsg,
// 				showCancel: false
// 			})
// 		},
// 		complete: (e) => {
// 			// activeTab.isLoading = false;
// 		}
// 	});
// 	
// 	});
// }

export {
	friendlyDate,
	// netHelper,
	queryTodayAction,
	queryTommorrowAction,
	queryPastActions,
	doLogin,
	doValidCode,
	doValidPhone,
	doRegist
}
